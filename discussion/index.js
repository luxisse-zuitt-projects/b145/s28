// Create a standard server using Node


// 1. Identify the ingredients/components needed in order to perform the task.

// http -> foundation of the server we're going to create; will allow Node JS to establish a connection in order to transfer data using the Hyper Text Transfer Protocol.
// require() -> this will allow us to acquire the resources needed to perform the task
let http = require('http')


// 2. Create a server using methods that we will take from the http resources

// the http module contains the createServer() method that will allow us to create a standard server set up.


// 3. Identify the procedure/task that the server will perform once the connection has been established.

// identify the communication between the client and the server
http.createServer( function(request, response) {
	response.end('Welcome to the Server, Batch 145 :)');
}).listen(3000)


// 4. Select an address/location where the connection will be established or hosted.


// 5. Create a response in the terminal to make sure if the server is running successfully.

console.log('Server is running successfully');
// careful when executing this command: npx kill-port <address>


// 6. Initialize a node package manager (NPM) in our project.


// 7. Install your first package/dependency from the NPM that will allow you to make certain tasks a lot more easier.
	// Adjacent Skills:
		// => install
		// => uninstall
		// => multiple installations
			//syntax 1: npm install <list of dependencies>
			// syntax 2: npm i <list of dependencies>

	// A. nodemon
	// B. express


// 8. Create a remote repository to backup the project.
	// NOTE: the node_modules should not be included in the files structure that will be saved online.
	
	// REPERCUSSIONS:
	// 1. It's going to take a longer time to stage/push your projects online.
	// 2. The node_modules will cause error upon deployment

	// USE .gitignore module.

	






